import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> requests<T>(
Future<T> Function() request,
Function(T) onResponse,
Future<void> Function(String) onError
)async{
  try{
    var response = await request();
    onResponse(response);
  }
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError(e.toString());
  }
  catch(e){
    onError(e.toString());
  }
}