import 'package:geolocator/geolocator.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class GeolocatorUseCase{
  Future<void> getCurrentPosition(
      Function(Position) onResponse,
      Future<void> Function(String) onError
      )async{
    LocationPermission permission = await Geolocator.checkPermission();
    if(!(await Geolocator.isLocationServiceEnabled())){
      onError('No');
      return;
    }
    if (permission == LocationPermission.denied || permission == LocationPermission.deniedForever){
      onError('Denied');
      return;
    }
    var position = await Geolocator.getCurrentPosition();
    onResponse(position);
  }

  Future<void> getCurrentAddress(
      Point point,
      Function(String) onResponse,
      Future<void> Function(String) onError
      )async{
    var response = await YandexSearch.searchByPoint(point: point, searchOptions: SearchOptions(
        searchType: SearchType.geo,
        geometry: false

    ));
    var result = await response.result;
    var firstResult = result.items?.firstOrNull?.name;
    if (firstResult == null){
      onError('Cant find');
      return;
    }
    onResponse(firstResult);
  }
}