import 'package:session0/main.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> signUp(email, fullname, password)async {
  final AuthResponse res = await supabase.auth.signUp(
    email: email,
    password: password,
    data: {
      'name': fullname
    }
  );
}