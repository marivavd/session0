import 'package:flutter/material.dart';

Future<void> showError(BuildContext context, String error)async{
  await showDialog(context: context, builder:(_){
    return AlertDialog(
      title: Text('Error'),
      content: Text(error),
      actions: [
        TextButton(onPressed: (){Navigator.of(context).pop();}, child: Text('Ok'))
      ],
    );
  });
}

void showLoading(BuildContext context){
  showDialog(context: context, barrierDismissible: false, builder:(_){
    return PopScope(
      canPop: false,
        child: Dialog(

          surfaceTintColor: Colors.transparent,
      backgroundColor: Colors.transparent,
      child: Center(child: CircularProgressIndicator(),),
    ));
  });
}

void hideLoading(BuildContext context){
  Navigator.of(context).pop();
}