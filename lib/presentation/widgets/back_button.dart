import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ButtonBack extends StatelessWidget {
  const ButtonBack({super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Navigator.of(context).pop();

      },
      child: Container(
        height: 44.w,
        width: 44.w,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(40)
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 16.25, horizontal: 19.25),
          child: SvgPicture.asset('assets/back.svg'),
        ),
      ),
    );
  }
}
