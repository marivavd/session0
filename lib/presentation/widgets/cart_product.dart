import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class CardProduct extends StatefulWidget {
  const CardProduct({super.key});

  @override
  State<CardProduct> createState() => _CardProductState();
}

class _CardProductState extends State<CardProduct> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16)
      ),
      child: Stack(
        children: [
          Padding(padding: EdgeInsets.only(left: 9.w, top: 34.w),child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(padding: EdgeInsets.symmetric(horizontal: 12.w),
              child: SizedBox(
                width: double.infinity,
                child: AspectRatio(
                  aspectRatio: 114.98/53.85,
                  child: Image.asset('assets/nike-zoom-winflo-3-831561-001-mens-running-shoes-11550187236tiyyje6l87_prev_ui 1.png'),
                ),
              )
              ),
              SizedBox(height: 12.6.w,),
              Text(
                'Best Seller',
                style: GoogleFonts.poppins(
                  textStyle: TextStyle(
                    fontSize: 12.sp,
                    height: 16/12.w,
                    color: Color(0xFF48B2E7),
                  )
                ),
              ),
              SizedBox(height: 8.w,),
              Text(
                'Nike Air Max',
                style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: 16.sp,
                      height: 20/16.w,
                      color: Color(0xFF6A6A6A),
                      fontWeight: FontWeight.w600
                    )
                ),
                maxLines: 1,
              ),
              Expanded(child: Align(alignment: Alignment.bottomCenter,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(child: Text(
                    '₽752.00',
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                            fontSize: 14.sp,
                            height: 16/14.w,
                            color: Color(0xFF2B2B2B),

                        )
                    ),
                  )),
                  GestureDetector(
                    child: Container(
                      height: 35.5.w,
                      width: 34.w,
                      decoration: BoxDecoration(
                        color: Color(0xFF48B2E7),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(14.w),
                          bottomRight: Radius.circular(14.w),
                        )
                      ),
                      padding: EdgeInsets.only(top: 7.5.w, bottom: 8.w, right: 7.w, left: 7.w),
                      child: SvgPicture.asset('assets/cart.svg'),
                      
                    ),
                  )
                ],
              ),))
            ],
          ),),
          Padding(padding: EdgeInsets.only(top: 3.w, left: 9.w),
            child: Container(
              height: 28.w,
              width: 28.w,
              decoration: BoxDecoration(
                  color: Color(0xFFF7F7F9),
                  borderRadius: BorderRadius.circular(40)
              ),
              padding: EdgeInsets.symmetric(vertical: 8.67, horizontal: 8),
              child: SvgPicture.asset('assets/heart.svg'),

            ),
          )
        ],
      ),
    );
  }
}
