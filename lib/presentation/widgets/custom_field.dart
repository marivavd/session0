import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomField extends StatefulWidget {
  final String label;
  final String hint;
  final TextEditingController controller;
  final bool enableObscure;
  final Function(String)? onChange;

  const CustomField({super.key, required this.label, required this.hint, required this.controller, this.enableObscure = false,  this.onChange});

  @override
  State<CustomField> createState() => _CustomFieldState();
}

class _CustomFieldState extends State<CustomField> {
  bool isObsure = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: GoogleFonts.raleway(
            textStyle: TextStyle(
              color: Color(0xFF1A2530),
              fontSize: 16.sp,
             height: 20/16.w,
              fontWeight: FontWeight.w500

            )
          )
        ),
        SizedBox(height: 12.w,),
        Container(
          width: double.infinity,
          height: 48.w,
          decoration: BoxDecoration(
            color: Color(0xFFF7F7F9),
            borderRadius: BorderRadius.circular(14),
            border: Border.all(color: Colors.transparent)
          ),
          child: TextField(
            controller: widget.controller,
            onChanged: widget.onChange,
            obscureText: (widget.enableObscure)?isObsure:false,
            obscuringCharacter: '*',
            style: GoogleFonts.raleway(
                textStyle: TextStyle(
                    color: Color(0xFF1A2530),
                    fontSize: 14.sp,
                    height: 16/14.w

                )
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.symmetric(horizontal: 14.w, vertical: 16.w),
              hintText: widget.hint,
              hintStyle: GoogleFonts.raleway(
                  textStyle: TextStyle(
                      color: Color(0xFF6A6A6A),
                      fontSize: 14.sp,
                      height: 16/14.w

                  )
              ),
              suffixIconConstraints: BoxConstraints(minWidth: 34.w),
              suffixIcon: (widget.enableObscure)?GestureDetector(
                onTap: (){
                  isObsure = !isObsure;
                },
                child: SvgPicture.asset('assets/eye.svg'),
              ):null
            ),
          ),
        )
      ],
    );
  }
}
