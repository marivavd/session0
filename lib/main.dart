import 'dart:async';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:session0/data/repository/request.dart';
import 'package:session0/data/repository/shared_preferense.dart';
import 'package:session0/presentation/dialogs.dart';
import 'package:session0/presentation/location.dart';
import 'package:session0/presentation/widgets/back_button.dart';
import 'package:session0/presentation/widgets/bottom_nav_bar.dart';
import 'package:session0/presentation/widgets/cart_product.dart';
import 'package:session0/presentation/widgets/custom_field.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';


Future<void> main() async {
  await Supabase.initialize(
    url: 'https://muozgansistgyudgofqc.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Im11b3pnYW5zaXN0Z3l1ZGdvZnFjIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTQ5ODgzNDcsImV4cCI6MjAzMDU2NDM0N30.7RdYIRhMPaOtQj4kcd1JYuhMQ-hrSgnlkXkA5TEZgk4',
  );
  WidgetsFlutterBinding.ensureInitialized();
  await initShare();

  runApp(MyApp());
}

// Get a reference your Supabase client
final supabase = Supabase.instance.client;

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver{
  FlutterLocalization localization = FlutterLocalization.instance;
  final connectivity = Connectivity();
  late StreamSubscription<List<ConnectivityResult>> results;

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    localization.init(
        mapLocales: [
          const MapLocale('ru', AppLocale.RU),
          const MapLocale('en', AppLocale.EN)
        ],
        initLanguageCode: (Platform.localeName.split('_').first != 'ru')?'en':'ru');
    localization.onTranslatedLanguage = (_) => setState(() {

    });
    results = connectivity.onConnectivityChanged.listen((event) {
      if(event == ConnectivityResult.none){
        showError(context, 'Internet creshed');
      }
    });
    super.initState();

  }
  @override
  void didChangeLocales(List<Locale>? locales) {
    // TODO: implement didChangeLocales
    if (locales == null){
      return;
    }
    localization.translate((locales.first.languageCode != 'ru')?'en':'ru');
    super.didChangeLocales(locales);

  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(375, 812),
      builder: (_, c){
        return  MaterialApp(
          title: 'Flutter Demo',
          supportedLocales: localization.supportedLocales,
          localizationsDelegates: localization.localizationsDelegates,
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
            useMaterial3: true,
          ),
          home: const MyHomePage(),
        );
      },
    );
  }
}


class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});


  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var controller = TextEditingController();
  int currentIndex = 0;
  List<Widget> sp = [Scaffold(backgroundColor: Color(0xFFF7F7F9)), Scaffold(backgroundColor: Color(0xFFF7F7F9)), Scaffold(backgroundColor: Color(0xFFF7F7F9)), Scaffold(backgroundColor: Color(0xFFF7F7F9))];
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          children: [
            SizedBox(height: 100,),
            CustomField(label: 'fhs', hint: 'vv', controller: controller)
          ],
        )
      )
    );
  }
}
